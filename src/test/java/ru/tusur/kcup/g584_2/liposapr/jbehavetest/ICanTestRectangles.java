package ru.tusur.kcup.g584_2.liposapr.jbehavetest;

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.JUnitStory;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;

public class ICanTestRectangles extends JUnitStory {

    /**
     * Sets story configuration.
     * @return Configuration that loads story with same name. Output is set to console and text files.
     */
    @Override
    public Configuration configuration() {
        return new MostUsefulConfiguration()
                .useStoryLoader(new LoadFromClasspath(this.getClass()))
                .useStoryReporterBuilder(new StoryReporterBuilder().withDefaultFormats().withFormats(Format.CONSOLE, Format.TXT));
    }

    /**
     * Configures story to use {@link RectangleSteps} as Steps provider.
     * @return InstanceStepsFactory configured to use {@link RectangleSteps} class.
     */
    @Override
    public InjectableStepsFactory stepsFactory() {
        return new InstanceStepsFactory(configuration(), new RectangleSteps());
    }
}
