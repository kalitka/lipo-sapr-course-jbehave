package ru.tusur.kcup.g584_2.liposapr.jbehavetest;


import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import ru.tusur.kcup.g584_2.liposapr.javaunitjbehave.Rectangle;

import static org.junit.Assert.assertEquals;

public class RectangleSteps {

    private Rectangle rect;
    private static final double delta = 1e-5;

    /**
     * Replaces rect field with a brand new default Rectangle.
     */
    @Given("a default rectangle")
    public void defaultRectangle() {
        rect = new Rectangle();
    }

    /**
     * Replaces rect field with a new Rectangle with given sizes.
     * @param width Rectangle's width
     * @param height Rectangle's height
     */
    @Given("a $width by $height rectangle")
    public void constructedRectangle(double width, double height) {
        rect = new Rectangle(width, height);
    }

    /**
     * Checks rect's area against known value.
     * @param area Expected area.
     */
    @Then("its area should be $area")
    public void theAreaShouldBe(double area) {
        assertEquals(rect.getArea(), area, delta);
    }

    /**
     * Checks rect's perimeter against known value.
     * @param perimeter Expected perimeter.
     */
    @Then("its perimeter should be $perimeter")
    public void thePerimeterShouldBe(double perimeter) {
        assertEquals(rect.getPerimeter(), perimeter, delta);
    }

    /**
     * Assigns rect's width to new value.
     * @param width Width to set.
     */
    @When("its width is set to $width")
    public void setWidthTo(double width) {
        rect.setWidth(width);
    }

    /**
     * Assigns rect's height to new value.
     * @param height Height to set.
     */
    @When("its height is set to $height")
    public void setHeightTo(double height) {
        rect.setHeight(height);
    }

    /**
     * Asserts that rect's width is equal to provided value.
     * @param width Expected width.
     */
    @Then("its width should be $width")
    public void theWidthShouldBe(double width) {
        assertEquals(rect.getWidth(), width, delta);
    }

    /**
     * Asserts that rect's height is equal to provided value.
     * @param height Expected height.
     */
    @Then("its height should be $height")
    public void theHeightShouldBe(double height) {
        assertEquals(rect.getHeight(), height, delta);
    }
}
