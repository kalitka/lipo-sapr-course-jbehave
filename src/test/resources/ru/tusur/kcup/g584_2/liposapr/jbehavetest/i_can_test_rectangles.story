Narrative:
in order to make my coursework I have to implement BDD testing
with jBehave.

Scenario: testing the default constructor

Given a default rectangle
Then its area should be 1
Then its perimeter should be 4


Scenario: testing non-default constructor

Given a 4 by 3 rectangle
Then its area should be 12
Then its perimeter should be 14


Scenario: testing non-integer values

Given a 3.45 by 7.0091 rectangle
Then its area should be 24.181395
Then its perimeter should be 20.9182

Scenario: testing setters

Given a default rectangle
When its width is set to 5.5
Then its width should be 5.5
Then its area should be 5.5
Then its perimeter should be 13
When its height is set to 4
Then its height should be 4
Then its area should be 22
Then its perimeter should be 19