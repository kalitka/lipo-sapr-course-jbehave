package ru.tusur.kcup.g584_2.liposapr.javaunitjbehave;

/**
 * Class for testing: basic rectangle with width and height fields;
 * area and perimeter are calculated.
 */
public class Rectangle {

    /**
     * Internal field to store height in.
     */
    private double mHeight;

    /**
     * Internal field to store width in.
     */
    private double mWidth;

    /**
     * Default constructor.
     * Makes 1×1 square.
     */
    public Rectangle() {
        mHeight = 1;
        mWidth = 1;
    }

    /**
     * Constructor with parameters.
     * @param width Width of the rectangle, must be greater than zero.
     * @param height Height of the rectangle, must be greater than zero.
     * @throws IllegalArgumentException when any of the parameters is non-positive
     */
    public Rectangle(double width, double height)
            throws IllegalArgumentException {
        this();
        setHeight(height);
        setWidth(width);
    }

    /**
     * Internal method to check whether value is positive.
     * @param value Value to check.
     * @return True when Value is positive, False otherwise.
     */
    private static boolean isPositive(double value) {
        if (value > 0) return true;
        else return false;
    }

    /**
     * Height getter.
     * @return Current height.
     */
    public double getHeight() {
        return mHeight;
    }

    /**
     * Height setter.
     * @param height New height.
     * @throws IllegalArgumentException when height is non-positive
     */
    public void setHeight(double height) throws IllegalArgumentException {
        if (!isPositive(height))
            throw new IllegalArgumentException("Height must be positive");
        mHeight = height;
    }

    /**
     * Width getter.
     * @return Current width.
     */
    public double getWidth() {
        return mWidth;
    }

    /**
     * Width setter.
     * @param width New width.
     * @throws IllegalArgumentException when width is non-positive
     */
    public void setWidth(double width) throws IllegalArgumentException {
        if (!isPositive(width))
            throw new IllegalArgumentException("Width must be positive");
        mWidth = width;
    }

    /**
     * Area calculation.
     * @return Area of the rectangle.
     */
    public double getArea() {
        return mWidth * mHeight;
    }

    /**
     * Perimeter calculation.
     * @return Perimeter of the rectangle.
     */
    public double getPerimeter() {
        return 2 * (mWidth + mHeight);
    }

    /**
     * toString() override.
     * @return String formatted as "(width * height) rectangle".
     */
    @Override
    public String toString() {
        return String.format("(%.3f * %.3f) rectangle", mWidth, mHeight);
    }
}
