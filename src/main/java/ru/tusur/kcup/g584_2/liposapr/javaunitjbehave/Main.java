package ru.tusur.kcup.g584_2.liposapr.javaunitjbehave;

public class Main {

	/**
	 * Main method of the program. Demonstrates basic usage of
	 * Rectangle class.
	 * @param args unused command-line arguments
	 */
	public static void main(String[] args) {

		System.out.println("Hello, jBehave world!");

		// same results as Rectangle(4, 3), but then we don't get to showcase
        // our beautiful setters
		Rectangle goodRect = new Rectangle();
		goodRect.setWidth(4);
		goodRect.setHeight(3);
		System.out.println(String.format("%s has area: %.2f; also perimeter: %.2f.",
				goodRect.toString(), goodRect.getArea(), goodRect.getPerimeter()));

		try {
			Rectangle badRect = new Rectangle(0, -100);
		} catch (IllegalArgumentException ex) {
			System.out.println(String.format("Just as planned, an exception: %s!!",
					ex.getMessage()));
		}
	}

}
