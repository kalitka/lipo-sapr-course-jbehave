# Проект на Java для работы с [jBehave](http://jbehave.org).  
Франкенштейн из проектов Eclipse, NetBeans, IntelliJ IDEA. А ещё собирается просто через Maven.

# Инструкции
В IDE импортируем как проекты на Maven (как родные уже не получится из-за мейвеновской структуры папок).  
Для сборки из командной строки в папке проекта:
```shell
# по идее, verify должен прогонять тесты
$ mvn verify
# если в конце простыни лога «BUILD SUCCESS», то всё ок и можно запустить
# (только зачем, тесты-то пройдены)
$ java -jar target/javaunitbase-0.1.0-jar-with-dependencies.jar
```
